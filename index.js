const express = require("express");
const index = express();

index.set("view engine", "ejs");
index.use(express.static(__dirname + "/public"));

index.get("/", function(req, res){
    res.render("home");
});

index.get("/resume", function(req, res){
    res.render("resume")
});

index.get("/myprojects", function(req, res){
    res.render("projects");
});

index.get("/about", function(req, res){
    res.render("about");
});

index.listen(3000, function(){
    console.log("server has started");
});